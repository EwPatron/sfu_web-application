// Берем все ссылки, якоря, изображения на странице
function getUrl() {
    console.log('Ссылки на странице: ' + Array.from(document.getElementsByTagName("a")).map(i => { return i.href }).join(" "))
    console.log('Якоря на странице: ' + Array.from(document.getElementsByTagName("a")).map(i => { return i.name }).join(" "))
    console.log('Изображения на странице: ' + Array.from(document.getElementsByTagName("img")).map(i => {return i.src}).join(" "))
}

// Делаем анимацию смены картинок
function animamationImage() {
    setInterval(() => {
        document.getElementById('mainImg').src = 'Images/Product/Product'+Math.floor(Math.random()*(5)+1)+'.jpg';
    }, 2000);
}

// Отслеживаем переход по ссылке
function watchClickHyperlink(id) {
    console.log('пользователь перешел по ссылке ' + document.getElementById(id).href)
}

// Отслеживаем нажатие кнопки
function watchClick() {
    console.log('Опа...он нажал на кнопку')
    document.getElementById('newButton').innerHTML = '<input value="И на меня тогда уж" onclick="console.log(\'И сюда нажал\')" type="button"></input>'
}