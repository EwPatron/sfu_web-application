// Считаем минимальный элемент в последовательности
function minArray() {
       var stroka = document.getElementById('firstTaskVariables').value         // Извлекаем текст из input
       var elements = takeNumberFromString(stroka)                     
       var min = elements[0]
       for (index = 0; index < elements.length; ++index) {                      // Находим минимум
              if (min > elements[index]) min = elements[index]
       }
       document.getElementById('firstTaskAnswer').innerHTML = min               // Выводим ответ
}

// Находим среднее
function averageValue() {
       var stroka = document.getElementById('secondTaskVariables').value
       var elements = takeNumberFromString(stroka)                     
       var average = 0
       for (index = 0; index < elements.length; ++index) {                                
              average += elements[index]                                        // Находим сумму последовательности
       }
       average = average/elements.length                                        // Находим среднее
       document.getElementById('secondTaskAnswer').innerHTML = average
}

// Находим кол-во максимума
function countMax() {
       var stroka = document.getElementById('thirdTaskVariables').value
       var elements = takeNumberFromString(stroka)                     
       var count = 0, max = elements[0]
       for (index = 0; index < elements.length; ++index) {                   // Находим кол-во максимумов 
              if (max < elements[index]) {
                     count = 0
                     max = elements[index]
              }
              if (max == elements[index]) count++ 
       }
       document.getElementById('thirdTaskAnswer').innerHTML = count
}

// Превращаем строку в массив чисел
function takeNumberFromString(stroka) {
       var massiv = [...stroka]                                                            // Делаем каждый символ массивом
       massiv.map(i => { if (isFinite(i) == true || i == ".") { return i } else { return " " } })    // Приводим к числу что можем
       var massiv2 = massiv.map(i => { if (isFinite(i) == true || i == ".") { return i } else { return " " } })
       var stroka2 = massiv2.join("")                                                      // Склеиваем строку
       stroka2.split(" ")                                                                  // Удаляем пробелы
       var massiv3 = stroka2.split(" ").filter(i => i != "")                               // Фильтруем массив
       var massiv4 = massiv3.map(i => Number(i))                                           // Приводим к числу
       return massiv4
}